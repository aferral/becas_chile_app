from src.constantes import COLS_DB,dtypes_db,TIME_FORMAT

str_base=["{0} {1}".format(x,dtypes_db[x]) for x in COLS_DB]
str_create_tables="create table if not exists becas ( {0} )".format(",".join(str_base))



def actualizar_db_sqlite(df,conn):
    df[COLS_DB].to_sql('becas', conn, if_exists='replace', index=False,dtype=dtypes_db)
