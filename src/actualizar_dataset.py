from datetime import datetime

import pandas as pd
import sqlite3
import os
from src.constantes import TIME_FORMAT, FOLDER_DATA
from src.scrapers.santander import extraer_data_actual
from src.scrapers.scraper_agci import extraer_data_agci
from src.utils_db import str_create_tables, actualizar_db_sqlite

os.makedirs(FOLDER_DATA,exist_ok=True)
PATH_DB=os.path.join(FOLDER_DATA,'becas_chile.db')
PATH_JSON=os.path.join(FOLDER_DATA,'becas_chile.json')


df_santander_actual=extraer_data_actual()
df_agci_actual=extraer_data_agci()

df_to_procesar=pd.concat([df_agci_actual,df_santander_actual],axis=0,ignore_index=True)

# abrir sqlite como dataframe
conn = sqlite3.connect(PATH_DB)
c=conn.cursor()
c.execute(str_create_tables)
df_db_historica = pd.read_sql("select * from becas",conn)

# definir indice real como fuente__id_source
indices_en_db=set(df_db_historica['id'].values.tolist())

# tomar ids de sqlite, tomar ids de dfs a procesar

indices_procesar=set(df_to_procesar['id'].values.tolist())

# solo deja los nuevos
indices_nuevos=indices_procesar - indices_en_db

print('Indices a agregar {0}'.format(len(indices_nuevos)))

df_nueva=df_to_procesar[df_to_procesar['id'].isin(indices_nuevos)]

# concadena y envia a sqlite
df_db_historica=pd.concat([df_db_historica,df_nueva],axis=0,ignore_index=True)

actualizar_db_sqlite(df_db_historica,conn)

conn.commit()
conn.close()

# escribe json tambine solo los activos
str_hoy=datetime.now().strftime(TIME_FORMAT)
df_db_historica=df_db_historica[df_db_historica.fin >= str_hoy]
df_db_historica.to_json(PATH_JSON,orient='records')

print("proceso finaliza exitosamente")
