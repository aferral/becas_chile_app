import requests
import pandas as pd
import os

from src.constantes import COLS_DB, TIME_FORMAT

url_to_use='https://api-manager.universia.net/becas-programs/api/search?page={0}&originCountry=CL&status=open'


def extraer_data_actual():
    total_pages=2
    current_page=1

    all_data=[]
    while current_page <= total_pages:
        json_data=requests.get(url_to_use.format(current_page)).json()
        assert(json_data['status'] == 200)

        all_data += json_data['data']['hits']
        total_pages=json_data["data"]['totalPages']
        total_pages = 0 if total_pages is None else total_pages 
        print("Extraccion de pagina {0} de {1}".format(current_page,total_pages))

        current_page += 1

    out=[]
    for x in all_data:
        id_source=x['id']
        nombre=x['name']
        descripcion=x['summary']
        inicio=x['start_date']
        fin=x['end_date']
        fuente='Becas Santander'
        url=os.path.join('https://app.becas-santander.com/es/program/',x['slug'])
        
        data={'id_source':id_source,'nombre':nombre,
                'descripcion':descripcion,'inicio':inicio,
                'fin':fin,'fuente':fuente,'url':url}
        out.append(data)

    df_out=pd.DataFrame(out)
    df_out['inicio']=pd.to_datetime(df_out['inicio']).dt.strftime(TIME_FORMAT)
    df_out['fin'] = pd.to_datetime(df_out['fin']).dt.strftime(TIME_FORMAT)
    df_out['id']=df_out['fuente']+"__"+df_out['id_source']
    df_out=df_out[COLS_DB]
    return df_out
