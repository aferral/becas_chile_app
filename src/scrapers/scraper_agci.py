import re
import os
import requests
import pandas as pd 
from bs4 import BeautifulSoup

from src.constantes import COLS_DB, TIME_FORMAT

url_agci='https://www.agci.cl/becas/becas-para-chilenos/convocatorias-vigentes#'


def extraer_data_agci():
    resp=requests.get(url_agci)
    raw=resp.content
    soup=BeautifulSoup(raw, "html.parser")


    html_with_table=soup.find("th",text=re.compile("Plazo Pos")).parent.parent


    mapa_id_to_url={}
    for ind,row in enumerate(html_with_table.find_all("tr")):
        id_row=None
        val_row=None
        if ind == 0 :
            continue
        for ind_col,col in  enumerate(row.find_all("td")):
            if ind_col == 0:
                id_row=col.text
            if ind_col == 2:
                z = col.find("span")
                url_rel=re.match("window.open\('(.*)'\)", z["onclick"])[1]
                val_row=os.path.join('https://www.agci.cl/',url_rel[1:])
        assert(id_row is not None)
        assert(val_row is not None)
        mapa_id_to_url[id_row]=val_row

    data=pd.read_html(str(html_with_table))
    assert(len(data) > 0)

    df_becas=data[0]
    df_becas['url']=[mapa_id_to_url[x] for x in df_becas['N°']]
    
    df_out=df_becas.copy()
    df_out['id_source']=df_out['N°']
    df_out['nombre']=df_out['Beca']
    df_out['descripcion']='-'
    df_out['inicio']=None
    df_out['fin']=df_out['Plazo Postulación']
    df_out['fuente']='agci'

    df_out['inicio']=pd.to_datetime(df_out['inicio'],format='%d/%m/%Y').dt.strftime(TIME_FORMAT)
    df_out['fin'] = pd.to_datetime(df_out['fin'],format='%d/%m/%Y').dt.strftime(TIME_FORMAT)
    df_out['id'] = df_out['fuente'] + "__" + df_out['id_source']
    df_out=df_out[COLS_DB]
    return df_out


if __name__ == '__main__':
    res=extraer_data_agci()




