export PYTHONPATH=$(pwd)

python src/actualizar_dataset.py
echo "Ended script. Starting git push"
git config --global user.email "docker@example.com"
git config --global user.name "docker run"

git add data_becas/becas_chile.json
git add data_becas/becas_chile.db
git commit -m "updated data $(date)"


git push https://aferral:$APP_PASS@bitbucket.org/aferral/becas_chile_app.git HEAD:master
echo "Ended git push OK"
