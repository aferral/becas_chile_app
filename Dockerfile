FROM python:3.8.16-slim-buster as builder
RUN apt-get update && apt-get install -y git libxml2-dev libxslt-dev python3-pandas zlib1g-dev

RUN python3 -m venv /venv
ENV PATH=/venv/bin:$PATH

COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt

RUN git clone https://aferral@bitbucket.org/aferral/becas_chile_app.git /app

ENV PATH=/venv/bin:$PATH


WORKDIR /app

CMD [ "bash", "entrypoint.sh" ]


