

install:
	pip install -r requirements.txt
update_data:
	python src/actualizar_dataset.py
	echo "Ended script. Starting git push"
	git add data_becas/becas_chile.json 
	git add data_becas/becas_chile.db
	git commit -m "updated data $(date)"
	git push origin master
	echo "Ended git push OK"

compile_front:
	cd frontend_vue && npm run build

build_docker_api:
	docker build  -t becas_chile_scraper:1.0.0 .
run_docker_api:
	docker run -env-file=.env  --rm becas_chile_scraper:1.0.0

