job "update_becas_docker" {
  periodic {
    cron             = "30 * * * * *"
    prohibit_overlap = true
  }
  datacenters = ["pi_serv"]

  type = "batch"

  group "batch_job" {
    count = 1
    
    task "install" {
      driver = "exec"

      # PARAMETROS FRECUENTEMENTE EDITADOS
      env {
        GRACE_SECONDS = 259200
        CRON_FREQ="30 * * * *"
        PYTHON_VER="python3.8"
        HTTPS_BITBUCKET="https://aferral@bitbucket.org/aferral/becas_chile_app.git"
      }

      lifecycle {
        hook    = "prestart"
      }

      config {
        command = "/bin/bash"
        args = ["-x","install.sh"]
      }
vault {
  policies = ["read_keys"]
}



# PUEDE SER NECESARIO EDITAR ESTA TEMPLATE
      template {
        data = <<EOT
{
"grace": {{env "GRACE_SECONDS"}},
"schedule": "{{env "CRON_FREQ"}}",
"api_key": "{{with secret "kv/hc"}}{{.Data.data.hc_api_key_write}}{{end}}",
"name": "{{env "NOMAD_JOB_PARENT_ID"}}",
"tags": "nomad cron_job",
"channels": "*",
"unique": ["name"]
}

EOT
destination = "${NOMAD_TASK_DIR}/hc_body.json"
}



template {
data = <<EOT
#!/bin/bash
set -e

GIT_URL=$(echo {{env "HTTPS_BITBUCKET"}} | sed "s/@/:{{with secret "kv/data/bitbucket"}}{{.Data.data.app_write_read}}{{end}}@/")


URL_HC=https://is-this-real-life.koomeba.com/api/v1/checks/
PYTHON_VER={{env "PYTHON_VER"}}

curl $URL_HC  --data @${NOMAD_TASK_DIR}/hc_body.json | jq -r .ping_url > ${NOMAD_TASK_DIR}/repo/url_to_call
mv ${NOMAD_TASK_DIR}/repo ${NOMAD_ALLOC_DIR}/repo

EOT
destination = "install.sh"
}
      }
    

task "run_job" {

driver = "docker"
config {
image="becas_chile_scraper:1.0.0"
}
template {
  data = <<EOH
APP_PASS="{{with secret "kv/bitbucket"}}{{.Data.data.app_write_read}}{{end}}"
EOH
destination = "secrets/file.env"
env         = true
}
}


    task "post_run" {
      driver = "exec"

      lifecycle {
        hook    = "poststop"
      }

      config {
        command = "/bin/bash"
        args = ["run.sh"]
      }
      template {
data = <<EOT

curl $(cat ${NOMAD_ALLOC_DIR}/repo/url_to_call)

EOT

destination = "run.sh"
}
}

}


}

